<?php
	require_once("./action/AJAX/AjaxLevelAction.php");

	$action = new AjaxLevelAction();
	$action->execute();

	echo json_encode($action->levelInfo);
