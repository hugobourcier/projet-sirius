<?php
	require_once("action/LoginAction.php");

	$action = new LoginAction();
	$action->execute();

	require_once("partial/header.php");
?>
	<script src="js/login.js" charset="utf-8"></script>
	<div id="menu_background">
		<div id="ampoule"></div>
		<div id="bolt"></div>
		<p id="logo">Thérémine Héros</p>
		<div id="login">
			<?php
				if (!empty($action->loginError)) {
				?>
				<div class="error"><?= $action->loginError ?></div>
				<?php
				}
				?>
			<form id="loginForm" action="login.php" method="post">
				<div class="log">
					<label>Nom d'usager :</label>
					<input id="inUsername" type="text" name="username" value="">
				</div>
				<div class="log">
					<label>Mot de passe :</label>
					<input type="password" name="password" value="">
				</div>
				<div class="log">
					<button type="submit">Entrer</button>
				</div>
		</div>
	</div>
<?php
	require_once("partial/footer.php");