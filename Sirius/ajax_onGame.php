<?php
	require_once("./action/AJAX/AjaxOnGameAction.php");

	$action = new AjaxOnGameAction();
	$action->execute();

	echo json_encode($action->gameInfo);
