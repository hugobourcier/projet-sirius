<?php
require_once("./action/AJAX/AjaxUserAction.php");

$action = new AjaxUserAction();
$action->execute();

echo json_encode($action->userInfo);
