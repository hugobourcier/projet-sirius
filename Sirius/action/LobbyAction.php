<?php
	require_once("action/CommonAction.php");
	require_once("./action/DAO/LobbyDAO.php");

	class LobbyAction extends CommonAction {

		public $levelError = "";


		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {

			if (!empty($_POST["levelId"])) {
				$levelError = "dans if";
				$data = [];
				$data["id"] = $_POST["levelId"];
				$data["key"] = $_SESSION["key"];
				$result = $this->callAPI("enter", $data);
				$rejoin = LobbyDAO::joinGame($result);

				if ($rejoin === "Success"){
					$_SESSION["visibility"] = CommonAction::$VISIBILITY_ONGAME;
				}

				$this->levelError = $rejoin;

			}

		}
	}