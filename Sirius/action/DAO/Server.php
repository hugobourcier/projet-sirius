<?php

class Server {

	public static $apiURL = "https://apps-de-cours.com/web-sirius/server/api/";

	public static function callAPI($service, array $data) {

		$newURL = Server::$apiURL . $service . ".php";

		$options = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query($data)
			)
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($newURL, false, $context);

		if (strpos($result, "<br") !== false) {
				var_dump($result);
				exit;
			}

		return json_decode($result);
	}



}