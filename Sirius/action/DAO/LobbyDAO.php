<?php

define('USER_ERROR', "Problème lié à l'utilisateur.");
define('GAME_ERROR', "Partie non trouvée.");
define('LEVEL_ERROR', "Niveau trop bas pour cette partie.");
define('GAME_FULL_ERROR', "La partie est pleine.");

class LobbyDAO {

	public static function joinGame($resultFromRejoin) {

		$result = "";

		if($resultFromRejoin === "GAME_ENTERED") {
			$result = "Success";
		} else {
			switch($resultFromRejoin) {
				case "EMPTY_KEY":
				case "EMPTY_ID":
				case "USER_IS_BANNED":
				case "USER_NOT_FOUND":
					$result = USER_ERROR;
					break;
				case "GAME_NOT_FOUND":
					$result = GAME_ERROR;
					break;
				case "EXCEEDED_LEVEL_RANGE":
					$result = LEVEL_ERROR;
					break;
				case "GAME_IS_FULL":
					$result = GAME_FULL_ERROR;
					break;
				default:
					$result = "Erreur.";
			}
		}

		return $result;
	}
}