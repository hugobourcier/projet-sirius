<?php

define('NO_USER_ERROR', "Utilisateur inexistant.");
define('BANNED_ERROR', "Utilisateur banni.");
define('USERNAME_PSWD_ERROR', "Combinaison nom d'utilisateur et mot de passe invalide.");

class LoginDAO {

	public static function authenticate($resultFromLogin) {

		$result = "";

		if (strlen($resultFromLogin) === 40) {
			$result = "Success";
			} else {
				switch($resultFromLogin) {
					case "USER_NOT_FOUND":
					case "CHARACTER_NOT_CREATED":
						$result = NO_USER_ERROR;
						break;
					case "USER_IS_BANNED":
					case "TOO_MANY_CONNECTIONS_BAN":
						$result = BANNED_ERROR;
						break;
					case "INVALID_USERNAME_PASSWORD":
						$result = USERNAME_PSWD_ERROR;
						break;
					default:
						$result = "Erreur.";
				}
			}

			return $result;
		}
	}
