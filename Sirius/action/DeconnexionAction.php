<?php
	require_once("action/CommonAction.php");

	class DeconnexionAction extends CommonAction {

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if (isset($_SESSION["key"])) {
				$data = array("key" => $_SESSION["key"]);
				$rep = $this->callAPI("signout", $data);
				session_unset();
				session_destroy();
			}
			header("location:login.php");
			exit;

		}
	}