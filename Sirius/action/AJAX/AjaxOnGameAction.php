<?php
	session_start();
	require_once("action/DAO/Server.php");

	$gameInfo = "";

	class AjaxOnGameAction {

		public function execute() {

			$data = [];
			$data["key"] = $_SESSION["key"];

			if ($_POST["type"] === "update") {
				$this->gameInfo = Server::callAPI("state", $data);
			} else {
				$data["skill-name"] = $_POST["attack"];
				$this->gameInfo = Server::callAPI("action", $data);
			}
		}
	}