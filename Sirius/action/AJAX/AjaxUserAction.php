<?php
	session_start();
	require_once("action/DAO/Server.php");

	$userInfo = "";

	class AjaxUserAction {

		public function execute() {
			$data = [];
			$data["key"] = $_SESSION["key"];
			$this->userInfo = Server::callAPI("user-info", $data);
		}
	}