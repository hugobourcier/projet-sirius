<?php
	session_start();
	require_once("action/DAO/Server.php");

	$levelInfo = "";

	class AjaxLevelAction {

		public function execute() {
			$data = [];
			$data["key"] = $_SESSION["key"];
			$this->levelInfo = Server::callAPI("list", $data);
		}
	}