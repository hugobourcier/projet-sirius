<?php
	require_once("./action/CommonAction.php");
	require_once("./action/DAO/LoginDAO.php");

	class LoginAction extends CommonAction {

		public $loginError = "";

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if (!empty($_POST["username"]) && !empty($_POST["password"])) {
				$data = [];
				$data["username"] = $_POST["username"];
				$data["pwd"] = $_POST["password"];
				$result = $this->callAPI("signin", $data);
				$authenticate = LoginDAO::authenticate($result);

				if ($authenticate === "Success") {
					$_SESSION["key"] = $result;
					echo $_SESSION["key"];
					$_SESSION["visibility"] = CommonAction::$VISIBILITY_MEMBER;
					header("location:lobby.php");
					exit;
				} else {
					$this->loginError = $authenticate;
				}
			}
		}
	}
