//---- GLOBALES ----//

let isBanned = false;
let attackAnimation = false;
let isCoolingDown = false;
let currentRequest = false;

let nbAllies = 0;
let alliesList = [];

let gameOverBool = false;

//---- ON LOAD ---//

window.onload = () => {

	spriteList = []
	initializeLevel();
	setTimeout(firstRequest, 3000);
}

	//créer les sprites liés à l'affichage initial
const initializeLevel = () => {

	let can = document.querySelector("canvas");
	ctx = can.getContext("2d");

	spriteList.push(new Background(0,0,BACKGROUND_IMG));
	spriteList.push(new Player(PLAYER_START_POS[0],PLAYER_START_POS[1], PLAYER_IMG));
	spriteList.push(new Arm(ARM_START_POS[0], ARM_START_POS[1], ARM_IMG));
	spriteList.push(new Background(CROWD_START_POS[0], CROWD_START_POS[1], CROWD_IMG));
	spriteList.push(new Curtains(0, 0, CURTAIN_LEFT_IMG, "left"));
	spriteList.push(new Curtains(CURTAIN_RIGHT_START_POS[0], CURTAIN_RIGHT_START_POS[1], CURTAIN_RIGHT_IMG, "right"));

	applause.play();

	tick();

}

//---- BOUCLES DES REQUÊTES ----//

	//première requête rend visible les boutons d'action
	//lance la boucle de requêtes principale
const firstRequest = () => {
	document.querySelector("#btnContainer").style.opacity = 1;
	document.querySelector("#infos").style.opacity = 1;
	updateActions();
	let intervalActions = setInterval(()=> {requestsInterval(intervalActions);}, 3000);
}

	//boucle de requêtes principale
const requestsInterval = (intervalActions) => {

	if (gameOverBool) {
		clearInterval(intervalActions);
	} else {
		updateActions();
	}
}

//----- REQUÊTES AJAX ----//

	//mise à jour des infos de la partie
	// 1. Si réponse n'est pas un objet : gérer erreur probable
	// 2. Si le jeu a attaqué, générer un ennemi
	// 3. Gérer les alliés
	// 4. Ajuster les barres de progrès en conséquence
const updateActions = () => {

	if (!gameOverBool) {

		$.ajax({
			url : "ajax_onGame.php",
			type: "POST",
			data: {
				type : "update"
			}
		})
		.done(response => {
			response = JSON.parse(response);

			if (typeof response !== "object") {requestError(response);}

			if (response.game.attacked) {spawnAgent();}
			manageAllies(response);
			ajustProgressBars(response);

		});
	}
}

	//le joueur fait une attaque
const attackAjax = (attackType) => {

	if (!isCoolingDown && !currentRequest) {

		displayMessage(MSG_ATTACK);

		currentRequest = true;

		$.ajax({
			url : "ajax_onGame.php",
			type: "POST",
			data : {
				type:  "attacking",
				attack : attackType
			}
		})
		.done(response => {
			displayMessage(MSG_NORMAL);
			response = JSON.parse(response);
			if (response === "OK") {
				attack(attackType);
				coolDown();
			} else {
				attackFailed(response);
			}

			currentRequest = false;
		});
	}
}

//---- GESTION DES ERREURS DE REQUÊTES ----//

const requestError = (response) => {
	let message = isGameOver(response);
	if (message != ""){
		gameOverBool = true;
		spawnAgent();
		setTimeout(()=>{popUpMessage(message, isBanned, endGame);}, 700);
	}
}

	//comparer la réponse avec les messages d'erreurs possibles
const isGameOver = (response) => {
	let message = "";

	switch(response) {
		case "GAME_NOT_FOUND_WIN":
			message = MSG_WIN
			lowerBar(document.querySelector("#GameLoss"),0);
			displayGameHP(0);
			break;
		case "GAME_NOT_FOUND_LOST":
			message = MSG_LOSS;
			lowerBar(document.querySelector("#HPLoss"),0);
			displayPlayerHP(0);
			break;
		case "GAME_NOT_FOUND_NONE":
			message = MSG_NOGAME;
			break;
		case "TOO_MANY_CALLS_BAN":
		case "USER_NOT_FOUND":
			message = MSG_BANNED;
			isBanned = true;
			break;
		default:
			break;
	}

	return message;
}

	//retour au lobby ou à la page principale
const endGame = () => {
	if (!isBanned) window.location.replace("lobby.php");
	else window.location.replace("deconnexion.php");
}

//---- GESTION DES ALLIÉS ----//

const manageAllies = (response) => {

	let allies = response.other_players;

	if (allies.length < nbAllies) {
		removeAlly(allies);
	}

	if (allies.length > 0) {

		for (let i = 0; i < allies.length;++i) {
			let name = allies[i].name;
			if (i > nbAllies-1) {
				newAlly(i, allies[i]);
			}
			if (allies[i].attacked != "--") {
				allyAttack(1);
			}
			alliesList[name].setHP(allies[i].hp);
		}
	}

}

const newAlly = (index, ally) => {

	let name = ally.name;

	let newClara = new Clara(CLARA_START_POS[0]+ALLIES_POS_MODIFIER[index],
							CLARA_START_POS[1]+ALLIES_POS_MODIFIER[index], CLARA_IMG,
							CLARA_SPEED, CLARA_LIMITS, ally.name, ally.hp);
	alliesList[name] = newClara;
	spriteList.splice(spriteList.length-3,0, alliesList[name]);
	nbAllies++;
}

	//animation d'attaque pour les alliées
const allyAttack = (nb) => {
	if (nb < 10) {
		spriteList.splice(spriteList.lenght-3, 0, new Note(CLARA_START_POS[0]+20, NOTE_IMG,PLAYER_START_POS[0]+20));
		spriteList.push(new Note(CLARA_START_POS[0]+20, NOTE_IMG,PLAYER_START_POS[0]-60));
		setTimeout(() => {allyAttack(nb+1);}, 50);
	}
}

const removeAlly = (allies) => {
	let newList = [];

	if (allies.lenght == 0) {
		alliesList = newlist;
	} else {
		for (let i = 0; i < allies.length; ++i) {
			let name = allies[i].name;
			newList[name] = alliesList[name];
		}

		allieslist = newList;
	}
	for (name in alliesList) {
		alliesList[name].setAlive(false);
	}
	nbAllies = alliesList.length;

}

//---- ATTAQUE ENNEMI ----//

const spawnAgent = () => {
	spriteList.splice(spriteList.length-1,0, new Agent(getRandom(100, 300), AGENT_START_POSY, AGENT_IMG));
}

//---- ATTAQUES JOUEUR ----//

const attack = (type) => {
	attackAnimation = true;

	switch (type) {
		case "Normal":
			spriteList.splice(spriteList.length-1,0, new Swans(SWANS_START_POS[0], SWANS_START_POS[1], SWANS_IMG));
			soundAttack1.play();
			break;
		case "Special1":
			spriteList.splice(spriteList.length-1,0, new Maria(MARIA_START_POS[0], MARIA_START_POS[1], MARIA_IMG));
			soundAttack2.play();
			break;
		case "Special2":
			spriteList.splice(spriteList.length-1,0, new Sun(SUN_START_POS[0], SUN_START_POS[1], SUN_IMG));
			soundAttack3.play();
		default:
			break;
	}
}

const attackFailed = (msg) => {

	let message = "";

	switch(msg) {
		case "NOT_ENOUGH_MP":
			message = MSG_NOMP;
			break;
		case "EMPTY_SKILL_NAME":
		case "SKILL_NOT_FOUND":
			message = MSG_NOSKILL;
			break;
		default:
			break;
	}

	addToMessage(message);
}

//---- AJUSTEMENT DES BARRES DE PROGRÈS ----//

const ajustProgressBars = (response) => {
	lowerBar(document.querySelector("#GameLoss"), percentageHealth(response.game.hp,response.game.max_hp));
	lowerBar(document.querySelector("#HPLoss"), percentageHealth(response.player.hp,response.player.max_hp));
	lowerBar(document.querySelector("#ManaLoss"), percentageHealth(response.player.mp,response.player.max_mp));

	displayPlayerHP(response.player.hp);
	displayPlayerMP(response.player.mp);
	displayGameHP(response.game.hp);
}

const percentageHealth = (current, max) => {
	return Math.round(current/max *100);
}

const lowerBar = (node, newPct) => {
	node.style.width = newPct + "%";
}

const displayPlayerHP = (hp) => {
	document.querySelector("#playerHPText").innerHTML = "Concentration : " + hp;
}

const displayPlayerMP = (mp) => {
	document.querySelector("#playerManaText").innerHTML = "MP : " + mp;
}

const displayGameHP = (hp) => {
	document.querySelector("#gameHPText").innerHTML = "Indifférence du public : " + hp;
}


//---- COOL DOWN ----//

const coolDown = () => {

	let div = document.querySelector("#btns");

	if (!isCoolingDown) {
		isCoolingDown = true;
		div.style.opacity = 0;
		addToMessage(MSG_COOLDOWN);
	} else {
		let op = parseFloat(div.style.opacity);
		op+=0.002;
		div.style.opacity = op;
		let h = op / 1 * 100;
	}

	if (div.style.opacity < 0.9) {
		setTimeout(() => {coolDown();}, 10);
	} else {
		displayMessage(MSG_NORMAL);
		isCoolingDown = false;
	}
}

//---- MESSAGES EN COURS DE PARTIE ----//

const displayMessage = (msg) => {
	document.querySelector("#messageBox").innerHTML = msg;
}

const addToMessage = (add) => {
	let message = document.querySelector("#messageBox").innerHTML;
	document.querySelector("#messageBox").innerHTML = message + add;
}