//---- VARIABLES GLOBALES ----//

let bulb = null;
let parent = null;

const soundEffect = new Audio();
soundEffect.src ="music/sound_buzz.mp3";

//---- ON LOAD ----//

window.onload = () => {

	parent = document.querySelector("#menu_background");
	bulb = document.querySelector("#ampoule");

	let textInput = document.querySelector("#inUsername");

	if(localStorage["previousUser"]) {
		textInput.value = localStorage["previousUser"];
	}

	document.querySelector("#loginForm").onsubmit = function(){
		localStorage["previousUser"] = textInput.value;
	};

	setInterval(flash, 100);
}

//---- ANIMATION ÉCLAIR ----//

const flash = () => {

	if (getRandom(100, 1) % 15 == 0) {
		soundEffect.play();
		let angle = getRandom(20, 0);

		let dir = Math.pow(-1, getRandom(3,1));
		angle = angle*dir;

		bulb.style.backgroundImage = "url('images/bulb_on.png')";

		let bolt = document.createElement("div");
		bolt.style.backgroundImage = 'url("images/bolt'+getRandom(2,1)+'.png")';
		bolt.setAttribute("id", "bolt");
		bolt.style.transform = 'rotate('+angle+'deg)';
		bolt.style.opacity = '1';
		parent.append(bolt);

		dis = setInterval(function() {dissipate(bolt);}, 60);

		setTimeout(function() {
			bulb.style.backgroundImage = "url('images/bulb_off.png')";
			parent.removeChild(bolt);
			clearInterval(dis);
		}, getRandom(300, 250));
	}
}

const dissipate = (e) => {

	let op = e.style.opacity-0.2;
	let to = e.style.top-2;

	e.style.opacity = op;
	e.style.top = to+"px";

}
