let spriteList = []
let ctx = null;

// tick principal
const tick = () => {

    if (ctx != null)
        ctx.clearRect(0,0, document.querySelector("#canvas").width, document.querySelector("#canvas").height);

    for (let i = 0; i < spriteList.length; i++) {
        const sprite = spriteList[i];
        alive = sprite.tick();

        if (!alive) {
            spriteList.splice(i,1);
            i--;
        }
	}

    window.requestAnimationFrame(tick);
}

//retourne un nombre aléatoire
const getRandom = (max, min) => {
	return Math.floor(Math.random()*max+min);
}

//afficher un pop up avec  un message
//func est la fonction qui sera appelée en appuyant sur le bouton
//banned est un booléen qui indique si l'utilisateur a été banni
const popUpMessage = (message, banned, func) => {

	document.querySelector("#popUp").style.display = "flex";
	document.querySelector("#mask").style.display = "block";
	document.querySelector("#messagePopUp").innerHTML = message;
	document.querySelector("#btnQuit").onclick = func;

	if (banned) document.querySelector("#btnQuit").innerHTML = "Déconnexion";
}

//fonctions pour gérer la déconnexion et le retour au lobby
const deconnect = () => {
	window.location.replace("deconnexion.php");
}
const leave = () => {
	window.location.replace("lobby.php");
}
