
//---- LIMITES DU JEU ----//

const RIGHT_FRONTIER = 950;
const LEFT_FRONTIER = 0;
const CANVAS_HEIGHT = 800;

//--- IMAGES POUR LES SPRITES ----//

const IMG_PATH_BACKGROUND = "./images/stage_bg.jpg";
const BACKGROUND_IMG = new Image();
BACKGROUND_IMG.src = IMG_PATH_BACKGROUND;

const IMG_PATH_PLAYER = "./images/player.png";
const PLAYER_IMG = new Image();
PLAYER_IMG.src = IMG_PATH_PLAYER;
const PLAYER_START_POS = [400, 300];

const IMG_PATH_ARM = "./images/arm.png";
const ARM_IMG = new Image();
ARM_IMG.src = IMG_PATH_ARM;
const ARM_START_POS = [438, 431];

const IMG_PATH_CROWD =  "./images/crowd_bg.png";
const CROWD_IMG = new Image();
CROWD_IMG.src = IMG_PATH_CROWD;
const CROWD_START_POS = [0, 550];

const IMG_PATH_CURTAIN =  "./images/rideau_bg.png";
const CURTAIN_LEFT_IMG = new Image();
CURTAIN_LEFT_IMG.src = IMG_PATH_CURTAIN
const CURTAIN_RIGHT_IMG = new Image();
CURTAIN_RIGHT_IMG.src = IMG_PATH_CURTAIN;
const CURTAIN_RIGHT_START_POS = [450, 0];

const IMG_PATH_AGENT = "./images/agent.png";
const AGENT_START_POSY = CANVAS_HEIGHT-200;
const AGENT_IMG = new Image();
AGENT_IMG.src = IMG_PATH_AGENT;

const IMG_PATH_EMBLEM = "./images/bulle.png";
const SOVIET_EMBLEM = new Image()
SOVIET_EMBLEM.src = IMG_PATH_EMBLEM;

const IMG_PATH_SWANS = "./images/swans.png";
const SWANS_IMG = new Image();
SWANS_IMG.src = IMG_PATH_SWANS;
const SWANS_START_POS = [400,300];

const IMG_PATH_MARIA = "./images/maria.png"
const MARIA_IMG = new Image();
MARIA_IMG.src = IMG_PATH_MARIA;
const MARIA_START_POS = [340, 125]

IMG_PATH_CLARA = "./images/clara.png"
const CLARA_IMG = new Image();
CLARA_IMG.src = IMG_PATH_CLARA;
const CLARA_START_POS = [100, 250];
const CLARA_SPEED = 0.5;
const CLARA_LIMITS = 15;
const ALLIES_POS_MODIFIER = [30,80,130,180];

const IMG_PATH_NOTE = "./images/notes.png";
const NOTE_IMG = new Image();
NOTE_IMG.src = IMG_PATH_NOTE;

const IMG_PATH_CLOUD = "./images/cloud.png";
const CLOUD_IMG = new Image();
CLOUD_IMG.src = IMG_PATH_CLOUD;

const IMG_PATH_SUN = "./images/sun.png";
const SUN_IMG = new Image();
SUN_IMG.src = IMG_PATH_SUN;
const SUN_START_POS = [500, 10];

//---- EFFETS SONORES ----//

const soundAttack1 = new Audio();
soundAttack1.src = "music/attack1.mp3";
const soundAttack2 = new Audio();
soundAttack2.src = "music/attack2.mp3"
const soundAttack3 = new Audio();
soundAttack3.src = "music/attack3.mp3";
const applause = new Audio();
applause.src = "music/applause.mp3";

//---- MESSAGES ----//

const MSG_WIN = "Partie remportée ! La foule a été conquise.";
const MSG_LOSS = "Partie perdue ! La foule a été rebutée.";
const MSG_NOGAME = "La partie est introuvable.";
const MSG_BANNED = "L'utilisateur est banni.";

const MSG_NORMAL = "Partie en cours...";
const MSG_NOSKILL = "Vous n'avez pas débloqué ce pouvoir.";
const MSG_NOMP = "Vous n'avez pas assez de MP.";
const MSG_ATTACK = "Pièce en cours de préparation...";
const MSG_COOLDOWN = "... Refroidissement ...";