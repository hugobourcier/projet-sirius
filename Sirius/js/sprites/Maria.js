class Maria extends BasicSprite {

    constructor(x,y,img) {
        super(x,y, img);
        this.opacity = 0;
        this.modifier = 1;
        this.count = 0;
    }

    tick() {

        let alive = true;

        this.opacity += 0.01*this.modifier;

        if (this.count == 2) {
            alive = false;
            attackAnimation = false;
        } else {
            if (this.opacity < 0.01 || this.opacity > 1) {
                this.count+=1;
                this.modifier*=-1;
            }
            ctx.globalAlpha = this.opacity;
            this.drawSelf();
            ctx.globalAlpha = 1;
        }

        return alive;
    }

}