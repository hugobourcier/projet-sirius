class BasicSprite {

	constructor(x,y,img) {
		if (this.constructor === BasicSprite) {
			throw new TypeError("Classe abstraite ne peut être instanciée.");
		} else {
			this.x = x;
			this.y = y;
			this.img = img;
		}
	}

	drawSelf() {
		if (this.img.complete) ctx.drawImage(this.img, this.x, this.y);
	}
}