
class Arm extends BasicSprite {
    constructor(x,y,img){
        super(x,y,img);
        this.xOrigin = x;
        this.yOrigin = y;
        this.speed = 0.5;
        this.limitUp = this.x+5;
        this.limitDown = this.x-5;
        this.modifier = -1;
    }

    tick() {
        
        if (attackAnimation) {
            this.x += this.speed*this.modifier;

            if (this.x >= this.limitUp || this.x <= this.limitDown) {
                this.modifier*=-1;
            }
        } else {
            this.x = this.xOrigin;
            this.y = this.yOrigin;
        }

        this.drawSelf();

        return true;

    }
}