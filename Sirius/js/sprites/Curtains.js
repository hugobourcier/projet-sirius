class Curtains extends BasicSprite {
	constructor (x,y,img, direction) {
		super(x,y,img);
		this.direction = direction == "left" ? -1 : 1;
		this.stopModifier = 600;
		this.speed = this.direction*3;
	}

	tick() {

		let alive = true;

		this.x += this.speed;

		if (this.x+this.stopModifier <= LEFT_FRONTIER || this.x >= RIGHT_FRONTIER) {
			alive = false;
		} else {
			this.drawSelf();
		}

		return alive;
	}
}