class Zeppelin {

	constructor(){
		this.x = (Math.floor(Math.random()*2+1) == 1 ? -200 : 1915);
		this.y = Math.floor(Math.random()*500+120);
		this.velocityX = (this.x == -200 ? 1 : -1);
		this.velocityY = 1;
		this.speed = Math.floor(Math.random()*5+3);
		this.peak = this.y + 5
		this.oscillation = 10

		this.node = document.createElement("div");
		this.node.setAttribute("id", "zeppelin");

		let bg = this.velocityX == 1 ? "2" : "";
		this.node.style.backgroundImage = "url(./images/zeppelin"+bg+".png)";

		this.setXY();

		document.querySelector("#lobby-background").append(this.node);
	}

	tick() {

		let alive = true;

		if (this.x < - 400 || this.x > 2300) {
			this.node.remove();
			alive = false;
			spawnZeppelin();
		} else {
			this.x = this.x + (this.velocityX*this.speed);

			this.setXY();

			this.velocityY *= -1;
		}

		return alive;
	}

	setXY() {
		this.node.style.left = this.x+"px";
		this.node.style.top = this.y + "px";
	}

}