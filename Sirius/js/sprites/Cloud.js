class Cloud extends BasicSprite {

	constructor(y,img, limit) {
		let x = getRandom(130, 90);
		let yModifier = getRandom(50,0);
		super(x,y-yModifier,img);
		this.speed = 3;
		this.limit = limit;
	}

	tick() {
		let alive = true;
		if (this.y < this.limit) {
			alive = false;
		} else {
			this.y-=this.speed;
			this.drawSelf();
		}

		return alive;
	}
}