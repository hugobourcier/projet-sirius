class Background extends BasicSprite {

	constructor (x,y, img) {
		super(x,y,img);
	}

	tick () {
		this.drawSelf();

		return true;
	}

}