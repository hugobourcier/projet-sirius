class Clara extends Bubble {

	constructor(x,y,img, speed, limits,name, hp) {
		super(x,y,img, speed, limits);
		this.name = name;
		this.noteList = [];
		this.noteCount = 0;
		this.noteMax = 6;
		this.allyInfos = new AllyInfos(this.x, name, hp)

		this.entrance = true;
		this.entranceCount = 0;
		this.maxEntrance = 6;

		this.alive = true;
	}

	tick() {

		if (!this.entrance) {
			this.y += this.speed*this.modifier;

			if (this.y > this.limitDown || this.y < this.limitUp)
				this.modifier*=-1;

			this.drawSelf();

			this.allyInfos.tick(this.y);
		} else {
			this.entranceAnimation();
		}
        return this.alive;
	}

	setHP(hp) {
		this.allyInfos.setHP(hp);
	}

	entranceAnimation(){
		if (this.entranceCount < this.maxEntrance) {
			spriteList.splice(spriteList.length-3,0, new Cloud(this.y+200, CLOUD_IMG, this.y));
			this.entranceCount++;
		} else this.entrance = false;
	}

	setAlive(alive) {
		this.alive = alive;
	}

	getName() {
		return this.name;
	}
}