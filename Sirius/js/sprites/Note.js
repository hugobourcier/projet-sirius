class Note extends BasicSprite {

	constructor(x, img,limit) {
		let y = getRandom(250, 200);
		super(x,y,img);
		this.speed = 5;
		this.limit = limit;
	}

	tick() {
		let alive = true;

		this.drawSelf();

		if (this.x > this.limit){
			alive = false;
		} else {
			this.x+=this.speed;
		}

		return alive;
	}

	getX() {
		return this.x;
	}

	getY() {
		return this.y;
	}
}