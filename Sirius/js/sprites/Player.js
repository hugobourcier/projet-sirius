class Player extends BasicSprite {

    constructor(x,y,img) {
        super(x,y,img);
    }

    tick() {

        let alive = true;
        this.drawSelf();

        return alive;
    }


}