class AllyInfos {

	constructor(x, name, hp) {
		this.x = x+60;
		this.yModifier = -10;
		this.name = name;
		this.hp = hp;
	}

	tick (y) {
		ctx.font = "15px Arial";
		ctx.fillStyle = "red";
		ctx.fillText(this.name + ", " + this.hp + " HP", this.x, y+this.yModifier);
	}

	setHP(hp) {
		this.hp = hp;
	}
}