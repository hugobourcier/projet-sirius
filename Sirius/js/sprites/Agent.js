class Agent extends BasicSprite {

    constructor(x,y,img) {
        super(x,y,img);
        this.speed = 1.5;
        this.direction = -1;
        this.maxHeight= 480;
        this.bubble = new Bubble(600, 250, SOVIET_EMBLEM, 2, 10);
        spriteList.splice(spriteList.length-2, 0, this.bubble);
    }

    tick() {

        let alive = true;

        if (this.y >= CANVAS_HEIGHT - 100) {
            alive = false;
            this.bubble.setAlive(false);
        } else {
            this.y += this.direction*this.speed;

            this.drawSelf();

            if (this.y <= this.maxHeight) {
                this.direction *= -1;
            }
        }

        return alive;
    }

}