class Bubble extends BasicSprite {

    constructor(x,y,img, speed, limits) {
        super(x,y,img);
        this.limitUp = this.y - limits;
        this.limitDown = this.y + limits;
        this.speed = speed;
        this.modifier = 1;
        this.alive = true;
    }

    tick() {

        this.y += this.speed*this.modifier;

        if (this.y > this.limitDown || this.y < this.limitUp)
            this.modifier*=-1;

        this.drawSelf();

        return this.alive;
    }

    setAlive(value) {
        this.alive = value;
    }
}