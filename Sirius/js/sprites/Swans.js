class Swans extends BasicSprite {

    constructor(x,y,img) {
        super(x,y,img);
        this.speedY = 2;
        this.speedX = 2;
    }

    tick(){
        let alive = true;
        
        if (this.y < 0) {
            alive = false;
            attackAnimation = false;
        } else {
            this.x-=this.speedX;
            this.y-=this.speedY;
            this.drawSelf();
        }

        return alive;
    }

}