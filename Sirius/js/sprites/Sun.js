class Sun extends BasicSprite {

    constructor(x,y,img) {
        super(x,y,img);
        this.angle = 0;
    }

    tick() {

        alive = true;

        if (this.angle > 3) {
            attackAnimation = false;
            alive =false;
        } else {
            this.angle+= (Math.PI / 180);
            ctx.rotate(this.angle);
            this.drawSelf();
            ctx.rotate(-this.angle);
        }

        return alive;
        
    }

}