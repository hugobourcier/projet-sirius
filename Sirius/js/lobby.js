//---- CONSTANTES ----//

const MSG_BANNED = "L'utilisateur a été banni.";

const sound = new Audio();
sound.src = "music/music_lobby.mp3";

const CITIES_X = [250, 550, 400, 200, 240, 600, 320, 700,
	740, 500, 570, 750, 730, 500, 530, 470];
const CITIES_Y = [150, 170, 50, 320, 250, 130, 150, 350,
	250, 270, 360, 30, 110, 390, 80, 150];

//---- VARIABLES GLOBALES ----//

let levelsCount = 0;
let pastLevel = null;
let pastCity = null;
let levelsCreated = false;
let userInfosCreated = false;
let synopsis = null;

//---- ON LOAD ----//

window.onload = () => {

	spriteList = []

	synopsis = document.createElement("img");
	synopsis.src = "images/synopsis.png";
	synopsis.setAttribute("id", "synopsis");
	document.querySelector("#main-lobby").append(synopsis);
	fadeIn(synopsis, 0,0);
	setInterval(() => {updateInfos();}, 3200);
}

//---- ANIMATION POUR LE SYNOPSIS ----//

const fadeIn = (element, deg, size) => {

	deg += 40;
	size += 0.80;

	element.style.width = size + "%";
	element.style.transform = "rotate("+deg+"deg)";

	if (deg < 6*360) {
		setTimeout(function() {fadeIn(element,deg,size);}, 50);
	} else {
		synopsis.onclick = function() {
			fadeOut(synopsis, 1);
		}
	}

}

const fadeOut = (element, op) => {

	op -= 0.01;

	if (op > 0) {
		element.style.opacity = op;
		setTimeout(function() {fadeOut(element,op);}, 10);
	} else {
		element.remove();
		openLobby();
	}
}

//--- AFFICHAGE DES ÉLÉMENTS DE LA SÉLECTION DU NIVEAU ----//

const openLobby = () => {

	sound.play();

	document.querySelector("#lobby-background").style.backgroundSize = "cover";
	document.querySelector("#map").style.opacity = 1;
	document.querySelector("#map").src = "images/usa_etats.png";
	document.querySelector("nav").style.opacity = 1;
	spriteList.push(new Zeppelin());
	initCities();
	tick();
}

	//afficher les villes sur la carte
const initCities = () => {

	let parent = document.querySelector("#main-lobby");
	for (let i = 0; i < levelsCount; ++i) {
		let node = document.createElement("img");
		node.src = "images/city.png";
		node.setAttribute("class","city");
		node.setAttribute("id", i);

		if (i < CITIES_X.length) {
			node.style.top = CITIES_Y[i] + "px";
			node.style.right = CITIES_X[i] + "px";
		} else {
			node.style.top = getRandom(250,150) + "px";
			node.style.right = getRandom(600, 175) + "px";
		}

		node.onclick = function(event) {
			if (pastLevel != null) {
				pastLevel.style.display = "none";
				pastCity.style.border = "none";
			}

			let e = document.getElementsByClassName("level")[event.target.id];
			e.style.display = "block";
			event.target.style.border = "3px solid red";
			pastLevel = e;
			pastCity =	event.target;
		};

		parent.appendChild(node);
	}
}

const spawnZeppelin = () => {
	spriteList.push(new Zeppelin());
}

//---- AFFICHAGE ET FERMETURE DES INFOS DU JOUEUR ----//

const getUserInfos = () => {

	document.querySelector("#mask").style.display = "block";
	$(".playerInfo").slideDown("slow", () => {});
}

const closeEl = (element) => {

	let node = element.parentNode;

	$(node).slideUp("slow", () => {
		node.style.display = "none";
		document.querySelector("#mask").style.display = "none";
	})
}

//--- REQUÊTES AJAX ----//

const updateInfos = () => {
	console.log("requête");
	levelInfos();
	userInfos();
}

const levelInfos = () => {

	$.ajax({
		url : "ajax_levels.php",
		type: "POST",
		data: {
		}
	})
	.done(response => {
		response = JSON.parse(response);
		checkErrors(response);
		displayLevelInfos(response);
	});
}

const userInfos = () => {

	$.ajax({
		url : "ajax_user.php",
		type : "POST",
		data : {
		}
	})
	.done(response => {
		response = JSON.parse(response);
		checkErrors(response);
		displayUserInfos(response);
	})
}

const checkErrors = (response) => {
	if (response === "TOO_MANY_CALLS_BAN" || response === "USER_NOT_FOUND") {
		popUpMessage(MSG_BANNED, true, deconnect);
	}
}

//---- AFFICHAGE INFOS ----//

const displayUserInfos = (response) => {

	if (!userInfosCreated) {
		createUserDivs();
	}

	let node = document.querySelector(".playerInfo");

	node.querySelector("#playerName").innerHTML = "Nom : " + response.username;
	node.querySelector("#playerType").innerHTML = "Type : " + response.type;
	node.querySelector("#playerLevel").innerHTML = "Niveau : " + response.level;
	node.querySelector("#playerHP").innerHTML = "HP : " + response.hp;
	node.querySelector("#playerVic").innerHTML = "Victoires : " + response.victories;
	node.querySelector("#playerMP").innerHTML = "MP : " + response.mp;
	node.querySelector("#playerDef").innerHTML = "Défaites : " + response.loss;
}

const createUserDivs = () => {
	let userDiv = document.querySelector("#playerTemplate").innerHTML;
	let parent = document.querySelector("#main-lobby");
	let node = document.createElement("div");
	node.innerHTML = userDiv;
	parent.appendChild(node);
	userInfosCreated = true;
}
const displayLevelInfos = (response) => {

	let count = 0;
	let node = null;

	if (!levelsCreated) {
		count = createLevelsDiv(response);
	}

	for (let i = 0; i < response.length;++i) {
		const element = response[i];
		node = document.getElementsByClassName("level")[i];
		node.querySelector("#levelName").innerHTML = "Nom : " + element.name;
		node.querySelector("#levelLevel").innerHTML = "Niveau : " + element.level;
		node.querySelector("#levelType").innerHTML = "Type : " + element.type;
		node.querySelector("#levelPlayers").innerHTML = "Joueurs : " + element.nb;
		node.querySelector("#levelHP").innerHTML = "HP : " + element.hp;
	}

	if (count > levelsCount) levelsCount = count;
}

const createLevelsDiv = (response) => {
	let count = 0;
	let node = null;
	let levelDiv = document.querySelector("#levelTemplate").innerHTML;
	let parent = document.querySelector(".levelList");
	for (let i = 0; i < response.length;++i) {
		node = document.createElement("div");
		node.innerHTML = levelDiv;
		node.querySelector(".level").setAttribute("id", response[i].id);
		parent.appendChild(node);
		count++;
	}
	levelsCreated = true;
	return count;
}


//---- REJOINDRE UNE PARTIE ----//

const playGame = (element) => {
	let form = element.parentNode;
	let div = form.parentNode;
	let id = div.id;
	form.querySelector("#levelId").value = id;
    form.submit();
}

const rejoinGame = () => {
	window.location.replace("onGame.php");
}
