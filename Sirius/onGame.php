<?php
	require_once("action/OnGameAction.php");

	$action = new OnGameAction();
	$action->execute();

	require_once("partial/header.php");
?>

	<script src="js/jquery.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/sprites/BasicSprite.js" charset="utf-8"></script>
	<script src="js/onGame_constants.js" charset="utf-8"></script>
	<script src="js/onGame.js" charset="utf-8"></script>
	<script src="js/sprites/Sun.js" charset="utf-8"></script>
	<script src="js/sprites/Bubble.js" charset="utf-8"></script>
	<script src="js/sprites/Swans.js" charset="utf-8"></script>
	<script src="js/sprites/Maria.js" charset="utf-8"></script>
	<script src="js/sprites/Agent.js" charset="utf-8"></script>
	<script src="js/sprites/Note.js" charset="utf-8"></script>
	<script src="js/sprites/AllyInfo.js" charset="utf-8"></script>
	<script src="js/sprites/Cloud.js" charset="utf-8"></script>
	<script src="js/sprites/Clara.js" charset="utf-8"></script>
	<script src="js/sprites/Player.js" charset="utf-8"></script>
	<script src="js/sprites/Arm.js" charset="utf-8"></script>
	<script src="js/sprites/Background.js" charset="utf-8"></script>
	<script src="js/sprites/Curtains.js" charset="utf-8"></script>

	<div id="mask"></div>

	<div id="popUp">
		<div id="messagePopUp">
		</div>
		<button type="button" id="btnQuit">Retour au lobby</button>
	</div>

	<div id="container">
		<div id="btnContainer">
			<div id="repertoire">Répertoire</div>
			<div id="btns">
				<div class="btnAttack" id="btnAttack1" onclick="attackAjax('Normal')">
				Le Cygne - Camille Saint-Saëns
				</div>
				<div class="btnAttack" id="btnAttack2" onclick="attackAjax('Special1')">
				Ave Maria - Franz Schubert
				</div>
				<div class="btnAttack" id="btnAttack3" onclick="attackAjax('Special2')">
				Summertime - George Gershwin
				</div>
			</div>
			<button class="btnOnGame" onclick="deconnect()">Déconnexion</button>
			<button class="btnOnGame" onclick="leave()">Retour au lobby</button>
			<div id="messageBox">Partie en cours...</div>
		</div>
		<div>
			<canvas id="canvas" width="950" height="800"></canvas>

			<template id="ally">
				<div id="allyInfos">
					<div class="allieName"></div>
					<div class="allieHP"></div>
				</div>
			</template>

			<div id="infos">

				<div class="lifeBar" id="playerHP">
					<p id="playerHPText"></p>
					<div id="HPLoss"></div>
				</div>
				<div class="lifeBar" id="playerMana">
					<p id="playerManaText"></p>
					<div id="ManaLoss"></div>
				</div>
				<div class="lifeBar" id="gameHP">
					<p id="gameHPText"></p>
					<div id="GameLoss"></div>
			</div>

		</div>

	</div>
		<!-- <div class="gameInfo" id="playerInfo"></div>
		<div class="gameInfo" id="progressInfo"></div> -->
	</div>
<?php
	require_once("partial/footer.php");