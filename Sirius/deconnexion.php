<?php
	require_once("action/DeconnexionAction.php");

	$action = new DeconnexionAction();
	$action->execute();

	header("location:login.php");
	exit;