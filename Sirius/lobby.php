<?php
	require_once("action/LobbyAction.php");

	$action = new LobbyAction();
	$action->execute();
	require_once("partial/header.php");
	echo $_SESSION["key"];
?>
	<script src="js/lobby.js" charset="utf-8"></script>¸
	<script src="js/sprites/zeppelin.js" charset="utf-8"></script>¸
	<div id="popUp">
		<div id="messagePopUp">
		</div>
		<button type="button" id="btnQuit">Retour au lobby</button>
	</div>
	<div id="lobby-background">
			<?php
				if (!empty($action->levelError)) {
					if ($action->levelError == "Success") {
						echo "<script>rejoinGame()</script>";
					} else {
				?>
				<div class="error"><?= $action->levelError ?></div>
				<?php
					}
				}
			?>
		<nav>
			<div id="menu">
				<ul>
					<li><a href="https://apps-de-cours.com/web-sirius/server">- Serveur -</a></li>
					<li onclick="getUserInfos()"> - Infos Personnage -</a></li>
					<li><a href="deconnexion.php">- Déconnexion -</a></li>
				</ul>
			</div>
		</nav>

		<div id="main-lobby">
			<img id="map">
				<div class="levelList">
					<template id="levelTemplate">
						<div class="level">
							<!-- <div id="form"> -->
							<form id="levelPost" action="lobby.php" method="post">
								<input type="hidden" name="levelId" id="levelId"/>
								<div id="levelName"></div>
								<div id="levelLevel"></div>
								<div id="levelType"></div>
								<div id="levelPlayers"></div>
								<div id="levelHP"></div>
								<button type="button" id="rejoindre" onclick="playGame(this)">Rejoindre !</button>
							</form>
							<!-- </div> -->
						</div>
					</template>
			<template id="playerTemplate">
			<div class="playerInfo">
					<img id="photoPlayer" src="images/photoPlayer.jpeg"/>
					<div id="playerName"></div>
					<div id="playerType"></div>
					<div id="playerHP"></div>
					<div id="playerMP"></div>
					<div id="playerLevel"></div>
					<div id="playerVic"></div>
					<div id="playerDef"></div>
					<button class="close" id="btnRetour" type="button" onclick="closeEl(this)">Retour</button>
				</div>
			</template>
			</div>
		</div>
	</div>
<?
require_once("partial/footer.php");